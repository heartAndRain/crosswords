/**
 * 定时刷新
 */

const request = require('request-promise-native');
const fs = require('fs');


(async () => {

  try {
    const r = await request('http://127.0.0.1/-/crosswords/rank/trigger', { method: 'POST'});
  } catch (error) {

    fs.appendFileSync('/root/crossword.rank.log', `
      ${(new Date()).toString()}:
      ${JSON.stringify(error,null, '\t')}
    `);
  }

})();
