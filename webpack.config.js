const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  entry: {
    'index': ['babel-polyfill','whatwg-fetch', './client//src/index.js']
  },
  output: {
    path: path.resolve(__dirname, 'client', 'dist'),
    publicPath: '/static/',
    filename: '[name].[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { modules: true }}
        ]
      },
      {
        test: /\.ttf$/,
        use: [{
          loader: 'file-loader'
        }]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
          },
          // {
          //   loader: 'image-webpack-loader',
          //   options: {
          //     mozjpeg: {
          //       progressive: true,
          //       quality: 65
          //     },
          //     // optipng.enabled: false will disable optipng
          //     optipng: {
          //       enabled: false,
          //     },
          //     pngquant: {
          //       quality: '65-90',
          //       speed: 4
          //     }
          //   }
          // }
        ],
      },
      {
        test: /\.jsonfile$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].json',
            }
          }
        ]
      }
    ]
  },
  plugins: (isProd => {
    const plgs = [
      new HtmlWebpackPlugin({
        template: './client/src/index.html'
      })
    ];

    if (!isProd) {
      plgs.push(new webpack.SourceMapDevToolPlugin({
        filename: '[name].[hash].js.map'
      }));
      plgs.push(new BundleAnalyzerPlugin());
    }

    if (isProd) {
      plgs.push(new UglifyJsPlugin());
      plgs.push(new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }));
    }
    return plgs;
  })(isProd)
};