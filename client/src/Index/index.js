import React from 'react';
import { Link } from 'react-router-dom';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from 'react-simple-maps';
import DocumentTitle from 'react-document-title';
import styles from './styles.css';
import Modal from '../Game/components/Modal';
import './china.jsonfile';
import wx from 'weixin-js-sdk';
import { Base64 } from 'js-base64';

export default class Index extends React.Component {

  constructor() {
    super();

    this.state = {
      title: '单词武林争霸',
      rules: '',
      mapScale: 0,
      mapWidth: 0,
      mapHeight: 0,
      provinceData: [],
      shareInfo: {
        show: false,
      },
    };

    this.shareConfig = {
      title: '学而思单词武林争霸',
      desc: '学而思单词武林争霸，等你来战',
      link: location.origin,
      imgUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1519814605216&di=6b0a82e90c310383a610e368ab8b176d&imgtype=0&src=http%3A%2F%2Fimg.25pp.com%2Fuploadfile%2Fsoft%2Fimages%2F2013%2F1117%2F20131117011855347.jpg'
    };
  }

  async componentDidMount() {
    let rootFontSize = document.documentElement.style.fontSize;
    if (!rootFontSize) return;
    rootFontSize = rootFontSize.slice(0, -2);
    const multiple = rootFontSize / 37.5;

    const { data: provinceData } = await this.getProvincePeople();
    const { data: indexInfo } = await this.getIndexInfo();
    const { data: userInfo } = await this.getUserInfo();
    this.setState({
      rules: indexInfo.game_rules,
      finalStart: indexInfo.final_start,
      isEnterFinal: userInfo.is_enter_final,
      mapScale: 400 * multiple,
      mapWidth: 324 * multiple,
      mapHeight: 280 * multiple,
      provinceData
    });


    const nonceStr = 'test';
    const timestamp = Math.floor(Date.now() / 1000);
    const url = encodeURIComponent(location.href);
    const { data: { signature } } = await this.getSignature(nonceStr, timestamp, url);
    wx.config({
      debug: false,
      appId: 'wx63322d4552e9e714',
      timestamp,
      nonceStr,
      signature,
      jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(() => {
      wx.onMenuShareTimeline({
        ...this.shareConfig
      });
      wx.onMenuShareAppMessage({
        ...this.shareConfig
      });
    });
    wx.error(res => {
      console.error(res);
    });

    const search = location.search.slice(1).split('&').reduce((obj, i) => { obj[i.split('=')[0]] = i.split('=')[1]; return obj; }, {});
    if (search.from && search.isappinstalled !== undefined && search.share) {
      const [nickname, score, level] = Base64.decode(search.share).split(',');

      this.setState({
        shareInfo: {
          show: true,
          nickname,
          score,
          level,
        }
      });
    }
  }

  getSignature(nonceStr, timestamp, url) {
    const params = `nonceStr=${nonceStr}&timestamp=${timestamp}&url=${url}`;
    return fetch(`/api/getSignature?${params}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getProvincePeople() {
    return fetch('/api/getProvincePeople', {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getIndexInfo() {
    return fetch('/api/getInfo', {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getUserInfo() {
    return fetch('/api/getUserInfo', { 
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getFillColorByPeopleNum(num) {
    if (num < 1) {
      return '#C9C8C7';
    } else if (num < 99) {
      return '#73E1FF';
    } else if (num < 999) {
      return '#FFAA00';
    } else if (num < 1999) {
      return '#FF6631';
    } else {
      return '#D80000';
    }
  }

  render() {
    return (
      <DocumentTitle title={this.state.title}>
        <div className={styles.container}>
          <Modal
            show={this.state.shareInfo.show}
            dialogStyle={{ height: '7rem' }}
            hideClose
            onClick={() => {
              this.setState({ shareInfo: { ...this.state.shareInfo, show: false }});
            }}
          >
            {
              <div className={styles.shareModal}>
                <div className={styles.shareScore}>{this.state.shareInfo.score}分</div>
                <div className={styles.shareText}>{this.state.shareInfo.nickname}的排名超过了<span>{this.state.shareInfo.level + '%'}</span>的玩家</div>
                <div style={{ top: '1.3rem', right: 0, height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '.5rem', right: '2rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '3rem', right: '.5rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '6.3rem', left: '1.5rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '5.5rem', left: '3rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '6.5rem', left: '3.5rem', height: '.3rem', width: '.3rem' }} className={styles.shareBgStar}></div>
                <div style={{ top: '1.5rem', left: '.4rem', height: '.45rem', width: '.45rem', transform: 'rotate(-20deg)' }} className={styles.shareBgStar}></div>
                <div style={{ top: '.5rem', left: '.4rem', height: '.5rem', width: '.5rem', transform: 'rotate(-30deg)' }} className={styles.shareBgStar}></div>
              </div>
            }
          </Modal>
          <div className={styles.header}></div>
          <div className={styles.content}>
            <div className={styles.photos}>
              <div className={styles.photo}></div>
              <div className={styles.photo}></div>
              <div className={styles.photo}></div>
              <div className={styles.photo}></div>
            </div>
            <div style={{ fontSize: '.3rem', marginTop: '.1rem'}}>本活动所含英语学习讲座请电脑登录<a style={{ color: '#6D0101'}} href="http://www.speiyou.com">学而思在线官网www.speiyou.com</a></div>
            <div className={styles.gameText}>
              公元二〇一八年，受名师点拨，单词武林中人才辈出，新的一届单词武林争霸呼之欲出，全国各路小英雄豪杰们纷纷来到“学而思单词武林争霸”代表自己的省份和城市争夺“全国单词武林霸主”之位……
            </div>
            <div className={styles.map}>
              {
                this.state.mapScale === 0
                  ?
                  null
                  :
                  (
                    <ComposableMap
                      projectionConfig={{
                        scale: this.state.mapScale,
                        // rotation: [-11,0,0],
                      }}
                      width={this.state.mapWidth}
                      height={this.state.mapHeight}
                      style={{
                        width: '100%',
                        height: 'auto',
                      }}
                    >
                      <ZoomableGroup center={[104, 36]} disablePanning>
                        <Geographies geography="/static/china.json">
                          {(geographies, projection) => geographies.map((geography, i) => {
                            let geoName = geography.properties.name;
                            let fill = '#C9C8C7';
                            if (geoName) {
                              const found = this.state.provinceData.find(d => d.province === geoName);
                              if (found) {
                                fill = this.getFillColorByPeopleNum(found.people_num);
                              }
                            }

                            const style = {
                              fill,
                              stroke: '#6D0101',
                              strokeWidth: 0.75,
                              outline: 'none',
                            };
                            return (
                              <Geography
                                key={i}
                                geography={geography}
                                projection={projection}
                                style={{
                                  default: style,
                                  hover: style,
                                  pressed: style,
                                }}
                              />
                            );
                          })}
                        </Geographies>
                      </ZoomableGroup>
                    </ComposableMap>
                  )
              }
              <div className={styles.mapLabel}>
                <div>中国地图</div>
                <div>
                  The map of China
                </div>
              </div>
            </div>
            <div>
              <div className={styles.subtitle}>比赛流程</div>
              <div className={styles.flow}></div>
            </div>
            <div className={styles.buttons}>
              <Link to="/board" className={styles.button}>查看擂台排名</Link>
              <a href="/try" className={styles.button}>小试牛刀</a>
              <a href="/game" className={styles.button}>代表我的城市出战</a>
              <div className={styles.button} onClick={() => {
                if (this.state.finalStart !== 1) {
                  alert('总擂台争霸赛已结束');
                  return;
                }
                if (!this.state.isEnterFinal) {
                  alert('您未入选总擂台争霸赛');
                  return;
                }
                location.href = '/game?final=true';
              }}>总擂台争霸赛</div>
              { 
                <Link to={{pathname: '/board', state: 'final'}} className={styles.button}>全国总擂台排名</Link>
              }
            </div>
            <div>
              <div className={styles.subtitle}>比赛规则</div>
              <div dangerouslySetInnerHTML={{ __html: this.state.rules }} className={styles.gameText}></div>
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}