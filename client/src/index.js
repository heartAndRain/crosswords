import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import Index from './Index/index';
import Board from './Board/index';
import Game from './Game/index';

import './global.css';

class Container extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <Route exact path="/" component={Index}/>
            <Route path="/board" component={Board}/>
            <Route path="/game" component={Game}/>
            <Route path="/try" component={Game}/>
          </div>
        </Router>
      </div>
    );
  }
}

ReactDOM.render(<Container />, document.getElementById('react-container'));
Raven.config('https://8da66a03ff8246b2a2afefcf5ee7fec5@sentry.io/298847').install();