import React from 'react';
import Keyboard from '../Keyboard';
import styles from './styles.css';
import classNames from 'classnames';


const _fetch = (requestPromise, timeout=30000) => {
  let timeoutAction = null;
  const timerPromise = new Promise((resolve, reject) => {
    timeoutAction = () => {
      reject('请求超时');
    };
  });
  setTimeout(()=>{
    timeoutAction();
  }, timeout);
  return Promise.race([requestPromise,timerPromise]);
};

export default class GameBoard extends React.Component {
  constructor(props) {
    super(props);

    this.MATRIX_LENGTH = 10;

    this.state = this.getInitState();

    this.allWords = [];
    this.wordPositionCache = {};

    this.handleFocus = this.handleFocus.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  getInitState() {
    return {
      loading: false,

      gameData: [],
      focusCell: { x: -1, y: -1 },

      // { 'xy': value }
      inputValues: {},

      moveMode: 'row',

      currentFillWord: [],

      correctWords: [],

      disabledPosition: [],


      descriptions: []
    };
  }

  componentWillReceiveProps(nextProps) {

    if (JSON.stringify(nextProps.gameData) !== JSON.stringify(this.props.gameData)) {

      // 清空
      this.allWords = [];
      this.wordPositionCache = {};
      const initInputValues = this.getInitInputValues(nextProps.gameData);
      // 重新初始化、合并
      this.setState({
        ...this.getInitState(), ...{
          gameData: nextProps.gameData.reduce((obj, item) => {
            const key = `${item.x}${item.y}`;
            obj[key] = item;

            // 收集所有单词
            item.words.forEach(word => {
              if (this.allWords.indexOf(word) === -1) this.allWords.push(word);
            });

            return obj;
          }, {}),
          inputValues: initInputValues.reduce((obj, item) => {
            const key = `${item.x}${item.y}`;
            obj[key] = item.value;
            return obj;
          }, {}),
          disabledPosition: initInputValues
        }
      });
    }
  }

  getInitInputValues(gameData) {
    return gameData.filter(() => this.getWhetherShowValue());
  }

  getWhetherShowValue() {
    const v = 10;
    const difficultV = 4;
    return (Math.random() * 100) % 10 < difficultV;
  }

  getDirectionText(dir) {
    return { 'col': '竖', 'row': '横' }[dir];
  }

  handleFocus(cellData) {

    if (this.props.gameFinish || this.state.loading) return;

    this.setFocus({ x: cellData.x, y: cellData.y });
    this.setState({
      descriptions: cellData.description.length === 2
        ? cellData.description.map((desc, index) => `${this.getDirectionText(cellData.wordsDirection[index])}：${desc}`)
        : cellData.description,
    });

    // set move mode
    if (cellData.wordsDirection.length === 1) {
      this.setState({ moveMode: cellData.wordsDirection[0] });
    }
    this.setState({ currentFillWord: cellData.words });
  }

  handleInputChange(value) {

    const { x, y } = this.state.focusCell;
    const isCorrectWordCell = this.state.disabledPosition.some(p => p.x === x && p.y === y);
    if (isCorrectWordCell || this.state.loading) return;

    if (x === -1 || y === -1) return;

    const cellData = this.state.gameData[`${x}${y}`];

    if (this.props.gameFinish) return;

    this.setValue(cellData.x, cellData.y, value, () => {
      console.log('words', cellData.words[0]);
      const words = cellData.words;
      if (words.length === 1) {
        if (this.isWordComplete(cellData.words[0])) {
          this.checkWordCorrect(cellData.words[0], () => {
            if (this.isBoardComplete()) {
              console.log('complete');
              this.props.onComplete && this.props.onComplete();
            }
          });
        }
      } else if (words.length === 2) {
        if (this.isWordComplete(cellData.words[0])) {
          this.checkWordCorrect(cellData.words[0], () => {
            if (this.isBoardComplete()) {
              console.log('complete');
              this.props.onComplete && this.props.onComplete();
            }
          });
        }

        if (this.isWordComplete(cellData.words[1])) {
          this.checkWordCorrect(cellData.words[1], () => {
            if (this.isBoardComplete()) {
              console.log('complete');
              this.props.onComplete && this.props.onComplete();
            }
          });
        }
      }


      // auto focus next
      if (this.state.moveMode === 'row' && this.isXFillAvailable(cellData)) {

        let offset = 1;

        while (this.isXFillAvailable({ x: cellData.x + offset - 1, y: cellData.y })) {
          // 跳过disabled位置
          const isCellDisable = this.state.disabledPosition.some(p => p.x === cellData.x + offset && p.y === cellData.y);
          if (isCellDisable) {
            offset++;
          } else {
            this.setFocus({ x: cellData.x + offset, y: cellData.y });
            break;
          }
        }

      } else if (this.state.moveMode === 'col' && this.isYFillAvailable(cellData)) {

        let offset = 1;

        while (this.isYFillAvailable({ x: cellData.x, y: cellData.y + offset - 1 })) {
          // 跳过disabled位置
          const isCellDisable = this.state.disabledPosition.some(p => p.x === cellData.x && p.y === cellData.y + offset);
          if (isCellDisable) {
            offset++;
          } else {
            this.setFocus({ x: cellData.x, y: cellData.y + offset });
            break;
          }
        }
      }
    });
  }

  checkWordCorrect(word, cb) {
    const wordPosition = this.wordPositionCache[word];
    const inputWord = wordPosition.reduce((str, p) => (str += this.state.inputValues[`${p.x}${p.y}`]), '');
    console.log('input', inputWord);

    this.setState({ loading: true });

    _fetch(fetch('/api/submitSolution', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        resolveWord: inputWord,
        maskWord: word,
      }),
      credentials: 'same-origin',
    }), 8000)
      .then(res => res.json())
      .then(res => {
        if (res.data.correct) {
          this.setState({
            correctWords: [...this.state.correctWords, word],
            disabledPosition: [...this.state.disabledPosition, ...wordPosition],
          }, () => {
            this.props.onCompleteOneWord && this.props.onCompleteOneWord(res.data.currentScore);
            cb();
            this.setState({ loading: false });
          });
        } else {
          cb();
          this.setState({ loading: false });
        }
      })
      .catch(e => {
        alert('网络不好，请重试一下~  ');
        this.setState({ loading: false });
        console.log(e.toString());
        Raven.captureException(e);
      });

  }

  isCellHasFilled({ x, y }) {
    return this.state.inputValues[`${x}${y}`];
  }

  isWordComplete(word) {

    let wordPosition = [];

    if (this.wordPositionCache[word]) {
      wordPosition = this.wordPositionCache[word];
    } else {
      wordPosition = this.props.gameData.filter(d => d.words.indexOf(word) !== -1)
        .map(d => ({ x: d.x, y: d.y }));
      // cache
      this.wordPositionCache[word] = wordPosition;
    }

    return wordPosition.every(d => this.state.inputValues[`${d.x}${d.y}`]);
  }

  isBoardComplete() {
    console.log(this.allWords.length, this.state.correctWords.length);
    console.log(this.props.gameData.length, Object.keys(this.state.inputValues).length);
    // return this.allWords.length !== 0 && this.allWords.length === this.state.correctWords.length;
    return this.props.gameData.length === Object.keys(this.state.inputValues).length;
  }

  isXFillAvailable({ x, y }) {
    if (x + 1 >= this.MATRIX_LENGTH) return false;
    const key = `${x + 1}${y}`;
    return Boolean(this.state.gameData[key]);
  }

  isYFillAvailable({ x, y }) {
    if (y + 1 >= this.MATRIX_LENGTH) return false;
    const key = `${x}${y + 1}`;
    return Boolean(this.state.gameData[key]);
  }

  setValue(x, y, value, cb) {

    const key = `${x}${y}`;
    const lastValue = this.state.inputValues[key] || '';
    let inputValue = value.split('').filter(w => w !== lastValue).join('');

    if (inputValue === '') inputValue = lastValue;

    this.setState({ inputValues: { ...this.state.inputValues, [key]: inputValue } }, cb);
  }

  setFocus({ x, y }) {
    this.setState({ focusCell: { x, y } });
    // const inputDom = document.getElementById( `position-${x}${y}`);
    // inputDom.focus();
  }

  renderDescription(desc) {
    return (
      <div key={desc} className={styles.description}>{desc}</div>
    );
  }

  renderCell(cellData, key) {
    if (!cellData) {
      return <div className={classNames([styles.cell, styles.boardNoWordCell])} key={key}></div>;
    }

    const { x, y, value, words } = cellData;
    const focusCell = this.state.focusCell;
    const isCorrectWordCell = this.state.disabledPosition.some(p => p.x === x && p.y === y);
    return (
      <div
        className={classNames([styles.cell, styles.boardWordCell], {
          [styles.cellForcus]: x === focusCell.x && y === focusCell.y,
          [styles.cellCorrect]: isCorrectWordCell
        })}
        key={key}
      >
        <div
          className={styles.cellInput}
          onClick={() => !isCorrectWordCell && this.handleFocus(cellData)}
        >
          {this.state.inputValues[`${x}${y}`] || ''}
        </div>
      </div>
    );
  }

  renderRow(xArray, yIndex) {
    const gameData = this.state.gameData;
    return <div className={styles.row} key={yIndex}>
      {
        xArray.map((cell, xIndex) => {
          const key = `${xIndex}${yIndex}`;
          return this.renderCell(gameData[key], key);
        })
      }
    </div>;

  }

  initArray(length, value) {
    const yArray = (new Array(10)).fill(value);
    return yArray.map(() => (new Array(length)).fill(value));
  }

  render() {

    const yArray = this.initArray(this.MATRIX_LENGTH, null);
    return (
      <div className={styles.board}>
        <div style={{ display: this.state.loading ? 'block' : 'none' }} className={styles.loading}></div>
        <div className={classNames([styles.finishMask], {
          [styles.finishMaskHide]: !this.props.gameFinish,
          [styles.finishMaskShow]: this.props.gameFinish,
        })}>
          <p>时间到</p>
        </div>
        <div className={styles.rows}>
          {yArray.map((xArray, yIndex) => {
            return this.renderRow(xArray, yIndex);
          })}
        </div>
        <div className={styles.descriptions}>
          {this.state.descriptions.map(desc => this.renderDescription(desc))}
        </div>
        <Keyboard style={{ marginTop: '0.25rem' }} onClick={w => this.handleInputChange(w)} />
      </div>
    );
  }
}