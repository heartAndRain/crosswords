import React from 'react';

import styles from './styles.css';

export default class Keyboard extends React.Component {

  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    if (e.target.textContent !== '') {
      this.props.onClick && this.props.onClick(e.target.textContent);
    }
  }

  render() {
    return (
      <div style={this.props.style} onClick={this.handleClick} className={styles.container}>
        <div className={styles.row}>
          <div className={styles.cell}>q</div>
          <div className={styles.cell}>w</div>
          <div className={styles.cell}>e</div>
          <div className={styles.cell}>r</div>
          <div className={styles.cell}>t</div>
          <div className={styles.cell}>y</div>
          <div className={styles.cell}>u</div>
          <div className={styles.cell}>i</div>
          <div className={styles.cell}>o</div>
          <div className={styles.cell}>p</div>
        </div>
        <div className={styles.row}>
          <div className={styles.cell}>a</div>
          <div className={styles.cell}>s</div>
          <div className={styles.cell}>d</div>
          <div className={styles.cell}>f</div>
          <div className={styles.cell}>g</div>
          <div className={styles.cell}>h</div>
          <div className={styles.cell}>j</div>
          <div className={styles.cell}>k</div>
          <div className={styles.cell}>l</div>
          <div className={styles.cell}></div>
        </div>
        <div className={styles.row}>
          <div className={styles.cell}>z</div>
          <div className={styles.cell}>x</div>
          <div className={styles.cell}>c</div>
          <div className={styles.cell}>v</div>
          <div className={styles.cell}>b</div>
          <div className={styles.cell}>n</div>
          <div className={styles.cell}>m</div>
          <div className={styles.cell}></div>
          <div className={styles.cell}></div>
          <div className={styles.cell}></div>
        </div>
      </div>
    );
  }
}