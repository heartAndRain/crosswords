import React from 'react';
import classNames from 'classnames';
import styles from './styles.css';

export default class Modal extends React.Component {
  render() {
    return (
      <div
        onClick={this.props.onClick}
        className={classNames([styles.container], {
          [styles.containerShow]: this.props.show,
        })}>
        <div style={this.props.dialogStyle} className={styles.dialog}>
          {this.props.hideClose ? null : <div onClick={() => {this.props.onClose && this.props.onClose(); }} className={styles.close}></div>}
          {this.props.children}
        </div>
      </div>
    );
  }
}