import React from 'react';
import DocumentTitle from 'react-document-title';
import GameBoard from './components/GameBoard';
import Modal from './components/Modal';
import styles from './styles.css';
import wx from 'weixin-js-sdk';
import { Base64 } from 'js-base64';
import qs from 'query-string';

export default class Game extends React.Component {

  constructor(props) {
    super(props);

    this.wxAppId = 'wx63322d4552e9e714';
    this.originPath = location.origin + location.pathname;

    // 是否是小试牛刀
    this.isTry = this.props.location.pathname === '/try';

    // 是否是总决赛
    this.isFinal = !!qs.parse(this.props.location.search).final === true;

    const getTitle = () => {
      if (this.isTry) return '小试牛刀';
      if (this.isFinal) return '总擂台争霸赛';
      return '单词武林争霸';
    };

    this.state = {
      title: getTitle(),
      gameTime: !this.isTry ? null : 300,
      gameStart: false,

      descriptions: [],
      allWords: [],
      gameData: [],
      gameFinish: false,

      nickname: '正在获取...',
      area: '地球',
      rank: 0,
      currentScore: 0,
      time: !this.isTry ? null : 300,

      resultModalShow: false,
      resultModalScore: 0,

      wordsShow: false,

      helpShow: !this.isTry,
      helpShowTime: 10,

      shareGuidMaskShow: false,

      isEnterFinal: null,
    };

    this.interval = null;
    this.helpShowInterval = null;

    this.getNewResolution = this.getNewResolution.bind(this);
    this.getUserInfo = this.getUserInfo.bind(this);
    this.handleGameStartOrStop = this.handleGameStartOrStop.bind(this);
    this.setInit = this.setInit.bind(this);


    this.shareConfig = {
      title: '学而思单词武林争霸',
      desc: '学而思单词武林争霸，等你来战',
      link: location.origin,
      imgUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1519814605216&di=6b0a82e90c310383a610e368ab8b176d&imgtype=0&src=http%3A%2F%2Fimg.25pp.com%2Fuploadfile%2Fsoft%2Fimages%2F2013%2F1117%2F20131117011855347.jpg'
    };
  }

  async componentDidMount() {

    this.getUserInfo();
    if (!this.isTry) {
      this.getIndexInfo();
    }


    this.helpShowInterval = setInterval(() => {
      if (this.state.helpShowTime === 0) {
        clearInterval(this.helpShowInterval);
        this.setState({ helpShow: false });
        return;
      }
      this.setState({ helpShowTime: --this.state.helpShowTime });
    }, 1000);


    const nonceStr = 'test';
    const timestamp = Math.floor(Date.now() / 1000);
    const url = encodeURIComponent(this.originPath);
    const { data: { signature } } = await this.getSignature(nonceStr, timestamp, url);
    wx.config({
      debug: false,
      appId: this.wxAppId,
      timestamp,
      nonceStr,
      signature,
      jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(() => {
      wx.onMenuShareTimeline({
        ...this.shareConfig
      });
      wx.onMenuShareAppMessage({
        ...this.shareConfig
      });
    });
    wx.error(res => {
      console.error(res);
    });
  }

  getSignature(nonceStr, timestamp, url) {
    const params = `nonceStr=${nonceStr}&timestamp=${timestamp}&url=${url}`;
    return fetch(`/api/getSignature?${params}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
    if (this.helpShowInterval) {
      clearInterval(this.helpShowInterval);
      this.helpShowInterval = null;
    }
  }

  async componentWillUpdate(nextProps, nextState) {
    if (this.state.shareGuidMaskShow !== nextState.shareGuidMaskShow) {
      if (nextState.shareGuidMaskShow) {
        const shareConfig = {
          title: `${this.state.nickname}打败了${this.getScoreLevel(this.state.resultModalScore) + '%'}的人`,
          link: `${location.origin}?share=${Base64.encodeURI(this.state.nickname + ',' + this.state.resultModalScore + ',' + this.getScoreLevel(this.state.resultModalScore))}`,
        };
        wx.ready(() => {
          wx.onMenuShareTimeline({
            ...this.shareConfig,
            ...shareConfig
          });
          wx.onMenuShareAppMessage({
            ...this.shareConfig,
            ...shareConfig
          });
        });
      } else {
        wx.ready(() => {
          wx.onMenuShareTimeline({
            ...this.shareConfig,
          });
          wx.onMenuShareAppMessage({
            ...this.shareConfig,
          });
        });
      }
    }
  }

  getUserInfo() {
    return fetch('/api/getUserInfo', { credentials: 'same-origin' })
      .then(res => res.json())
      .then(({ data }) => {
        this.setState({ nickname: data.nickname, area: data.province, rank: data.rank, final_rank: data.final_rank, isEnterFinal: data.is_enter_final });
      })
      .catch(e => Raven.captureException(e));
  }

  getIndexInfo() {
    return fetch('/api/getInfo', {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .then(({ data }) => {
        this.setState({ gameTime: data.game_time, time: data.game_time });
      })
      .catch(e => Raven.captureException(e));
  }

  getNewResolution() {
    const url = !this.isTry ? '/api/getResolution' : '/api/getTryResolution';
    return fetch(url, { credentials: 'same-origin' })
      .then(re => re.json())
      .then(res => res.data.resolution)
      .catch(e => Raven.captureException(e));
  }

  getAllWords() {
    fetch(!this.isTry ? '/api/getAllWords' : '/api/getAllTryWords', { credentials: 'same-origin' })
      .then(res => res.json())
      .then(({ data }) => {
        this.setState({ allWords: data });
      })
      .catch(e => Raven.captureException(e));
  }

  gameStart() {
    return fetch('/api/gameStart', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin',
      body: JSON.stringify({
        isTry: this.isTry
      })
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  gameFinish() {
    return fetch('/api/gameFinish', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin',
      body: JSON.stringify({
        isTry: this.isTry,
        isFinal: this.isFinal
      })
    })
      .then(res => res.json())
      .then(res => res.data.success)
      .catch(e => Raven.captureException(e));
  }

  setInit() {
    this.setState({
      gameData: [],
      descriptions: [],
      currentScore: 0,
      time: this.state.gameTime,
    });
  }

  async handleGameStartOrStop() {
    if (this.state.gameTime === null) return;

    if (!this.state.gameStart && !this.interval) {

      try {
        const res = await this.gameStart();

        if (!res.data.success) {
          throw new Error();
        }

        const resolution = await this.getNewResolution();

        this.setState({
          gameData: resolution,
          gameStart: true,
          gameFinish: false,
        }, () => {
          // 开启计时器
          if (this.interval) return;
          this.interval = setInterval(async () => {
            // 时间到
            if (this.state.time === 0) {
              const finish = await this.gameFinish();

              this.setState({
                gameFinish: true,
                gameStart: false,
                resultModalShow: true,
                resultModalScore: finish.score
              }, () => {
                this.setInit();
              });

              clearInterval(this.interval);
              this.interval = null;
              return;
            }

            this.setState({
              time: --this.state.time
            });
          }, 1000);
        });
      } catch (error) {
        alert('游戏异常，请尝试重新开始');
        return;
      }
    } else {
      // 结束游戏
      const finish = await this.gameFinish();

      // 归零
      this.setState({
        gameStart: false,
        resultModalShow: true,
        resultModalScore: finish.score
      }, () => {
        this.setInit();
      });
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  getScoreLevel(score) {
    if (score < 1000) {
      return Math.floor(+score / 1500 * 100) === 0 ? 1 : Math.floor(+score / 1500 * 100);
    } else if (score >= 1000 && score < 1500) {
      return 98;
    } else if (score > 1500) {
      return 99;
    }
  }

  formatTime(sec) {
    const min = Math.floor(sec / 60) + '';
    const fSec = sec % 60 + '';
    return `${min.length === 1 ? '0' + min : min}:${fSec.length === 1 ? '0' + fSec : fSec}`;
  }

  render() {

    if (this.isFinal && this.state.isEnterFinal !== null && !this.state.isEnterFinal) {
      alert('您未入选总擂台争霸赛');
      location.href = '/';
      return null;
    }

    return (
      <DocumentTitle title={this.state.title}>
        <div className={styles.container}>
          <Modal
            show={this.state.helpShow}
            hideClose
            dialogStyle={{ height: '7rem' }}
          >
            <div className={styles.helpText}>
              <span className={styles.helpTextTitle}>大侠，参赛请注意：</span><br />
              1、总时长为20分钟，不可暂停，前一页答完则自动出下一页；<br />
              2、如对成绩不满意，可找时间再次发起挑战，挑战次数不限；<br />
              3、如有单词不会拼，可点击单词方格出提示，提示次数不限；<br />
              4、如有单词拼写错，不积分不扣分，完成本页开启下页答题；<br />
              <span className={styles.helpTextTitle}>祝大侠好运</span>
              <p className={styles.helpCloseText}>关闭({this.state.helpShowTime}s)</p>
            </div>
          </Modal>
          <Modal
            show={this.state.wordsShow}
            onClose={() => this.setState({ wordsShow: false })}
          >
            <div className={styles.allWordContainer}>

              {this.state.allWords.length ? 
                this.state.allWords.map((item,index) => <div className={styles.allWord} key={index}><span className={styles.word}>{item.word}:</span>&nbsp;{item.desc}</div>)
                : <div style={{ textAlign: 'center', fontSize: '.5rem', color: '#fff'}}>加载中...</div>
              }
            </div>
          </Modal>
          <Modal
            show={this.state.resultModalShow}
            onClose={() => {
              this.setState({ resultModalShow: false });
            }}
            onClick={this.isTry ? () => {
              this.props.history.replace({ pathname: '/' });
            } : () => {
              // this.setState({ resultModalShow: false });
            }}
            dialogStyle={this.isTry ? { background: 'none', width: '10rem' } : { height: '7rem' }}
          >
            {
              !this.isTry
                ?
                <div className={styles.shareModal}>
                  <div className={styles.shareScore}>{this.state.resultModalScore}分</div>
                  <div className={styles.shareText}>你的排名已经超过<span>{`${this.getScoreLevel(this.state.resultModalScore)}%`}</span>的玩家</div>
                  <div className={styles.shareButtons}>
                    <div onClick={() => {
                      this.setState({ resultModalShow: false, shareGuidMaskShow: true });
                    }} className={styles.shareButton}>
                      <div className={styles.shareFriendsIcon}></div>
                      <div style={{ textAlign: 'center' }}>
                        <p>分享给朋友</p>
                        <p>搬救兵</p>
                      </div>
                    </div>
                    <div onClick={() => {
                      this.setState({ resultModalShow: false, shareGuidMaskShow: true });
                    }} className={styles.shareButton}>
                      <div className={styles.sharePyqIcon}></div>
                      <div style={{ textAlign: 'center' }}>
                        <p>分享到朋友圈</p>
                        <p>嘚瑟一下</p>
                      </div>
                    </div>
                  </div>
                  <div style={{ top: '.8rem', right: 0, height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '.5rem', right: '2rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '3rem', right: '.5rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '4.3rem', left: '1.5rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '4.2rem', left: '3rem', height: '.45rem', width: '.45rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '4.8rem', left: '3.5rem', height: '.3rem', width: '.3rem' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '1.5rem', left: '.4rem', height: '.45rem', width: '.45rem', transform: 'rotate(-20deg)' }} className={styles.shareBgStar}></div>
                  <div style={{ top: '.5rem', left: '.4rem', height: '.5rem', width: '.5rem', transform: 'rotate(-30deg)' }} className={styles.shareBgStar}></div>
                </div>
                :
                <div className={styles.tryModal}></div>
            }

          </Modal>
          <Modal
            show={this.state.shareGuidMaskShow}
            hideClose
            dialogStyle={{ background: 'none' }}
            onClick={() => {
              this.setState({ shareGuidMaskShow: false });
            }}
          >
            <div className={styles.shareGuide}>
              点击右上角分享哦~
            </div>
          </Modal>
          <div className={styles.gameHeader}>
            <div className={styles.username}>{this.state.nickname}</div>
            <div className={styles.gameInfo}>
              <div>
                {
                  this.isTry
                    ? null
                    :  (this.isFinal ? <div style={{ marginRight: '.53rem' }}>
                      <span>名次：</span>
                      <span className={styles.textBlue}>{this.state.rank ? `第${this.state.final_rank}名` : '暂无排名'}</span>
                    </div> : <div style={{ marginRight: '.53rem' }}>
                      <span>{this.state.area}：</span>
                      <span className={styles.textBlue}>{this.state.rank ? `第${this.state.rank}名` : '暂无排名'}</span>
                    </div>)
                }
                <div>
                  <span>成绩：</span>
                  <span className={styles.textBlue}>{this.state.currentScore}分</span>
                </div>
              </div>
              <div>
                <div>
                  <span>倒计时：</span>
                  <span className={styles.textRed}>{this.state.time === null ? '加载中...' : this.formatTime(this.state.time)}</span>
                </div>
              </div>
            </div>
          </div>
          <GameBoard
            gameFinish={this.state.gameFinish}
            gameData={this.state.gameData}
            onFocus={focusData => {
              this.setState({ descriptions: focusData.description });
            }}
            onCompleteOneWord={currentScore => {
              this.setState({ currentScore });
            }}
            onComplete={async () => {
              const resolution = await this.getNewResolution();
              this.setState({ descriptions: [], gameData: resolution });
            }}
          />
          <div className={styles.footer}>
            <button onClick={() => {
              this.getAllWords();
              this.setState({ wordsShow: true });
            }}>查看比赛单词</button>
            <div className={styles.seperator}></div>
            <button onClick={this.handleGameStartOrStop}>{this.state.gameStart ? '结束' : '开始'}比赛</button>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}