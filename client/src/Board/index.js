import React from 'react';
import styles from './styles.css';
import { Link } from 'react-router-dom';
import throttle from 'lodash/throttle';
import DocumentTitle from 'react-document-title';
import InfiniteScroll from 'react-infinite-scroller';

export default class Board extends React.Component {

  constructor(props) {
    super(props);

    const isFinal = props.location.state === 'final';
    this.limit = isFinal ? 30 : 15;
    this.state = {
      user: {
        nickname: '',
        province: '',
        area_code: '',
        rank: 0,
      },
      list: [],
      title: !isFinal ? '全国总排名' : '总决赛排名',
      page: 'country',

      listIsLoading: false,
      offset: 0,
      hasMore: true,

      isFinal,
    };

    this.getCountryRankList = this.getCountryRankList.bind(this);
    this.getRankListBetweenCity = this.getRankListBetweenCity.bind(this);
    this.getPersonRankList = this.getPersonRankList.bind(this);
    this.renderHead = this.renderHead.bind(this);
    this.handleOnBottom = throttle(this.handleOnBottom, 1000).bind(this);
  }

  getUserInfo() {
    return fetch('/api/getUserInfo?', {
      credentials: 'same-origin',
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getCountryRankList(offset) {
    return fetch(`/api/getCountryRankList?limit=${this.limit}&offset=${offset}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getRankListBetweenCity(offset) {
    return fetch(`/api/getRankListBetweenCity?limit=${this.limit}&offset=${offset}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getPersonRankList(offset) {
    return fetch(`/api/getCityRankList?city=${this.state.user.area_code}&limit=${this.limit}&offset=${offset}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  getFinalRankList(offset) {
    return fetch(`/api/getFinalList?limit=${this.limit}&offset=${offset}`, {
      credentials: 'same-origin'
    })
      .then(res => res.json())
      .catch(e => Raven.captureException(e));
  }

  async componentDidMount() {

    this.setState({ listIsLoading: true });

    try {
      const { data: { nickname, province, area_code, rank, final_rank, is_enter_final } } = await this.getUserInfo();
      // const list = await this.getCountryRankList(0);
      this.setState({
        user: {
          nickname,
          province,
          rank,
          final_rank,
          area_code,
          is_enter_final,
        },
        // list: list.data,
        // listIsLoading: false,
        // offset: this.state.offset + this.limit
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentWillUnmount() {

  }

  async handleOnBottom() {
    console.log('bo');
    if (!this.state.listIsLoading && this.state.hasMore) {
      this.setState({ listIsLoading: true });

      if (this.state.isFinal) {
        // 总决赛排名
        const list = await this.getFinalRankList(this.state.offset);
        if (list.data.length === 0) {
          this.setState({ listIsLoading: false, hasMore: false });
          return;
        }
        this.setState({
          list: this.state.list.concat(list.data),
          offset: this.state.offset + this.limit,
          listIsLoading: false,
        });
      } else if (this.state.page === 'country') {
        // 全国排名
        const list = await this.getCountryRankList(this.state.offset);
        if (list.data.length === 0) {
          this.setState({ listIsLoading: false, hasMore: false });
          return;
        }
        this.setState({
          list: this.state.list.concat(list.data),
          offset: this.state.offset + this.limit,
          listIsLoading: false,
        });
      } else if (this.state.page === 'person') {
        // 我的城市排名
        const list = await this.getPersonRankList(this.state.offset);
        if (list.data.length === 0) {
          this.setState({ listIsLoading: false, hasMore: false });
          return;
        }
        this.setState({
          list: this.state.list.concat(list.data),
          offset: this.state.offset + this.limit,
          listIsLoading: false,
        });
      } else if (this.state.page === 'city') {
        // 城市间排名
        const list = await this.getRankListBetweenCity(this.state.offset);
        if (list.data.length === 0) {
          this.setState({ listIsLoading: false, hasMore: false });
          return;
        }
        this.setState({
          list: this.state.list.concat(list.data),
          offset: this.state.offset + this.limit,
          listIsLoading: false,
        });
      }
    }
  }

  timeFormater(msec) {
    const date = new Date(msec);
    let m = (date.getMonth() + 1) % 12;
    let d = date.getDate();
    m = m > 9 ? m : '0' + m;
    d = d > 9 ? d : '0' + d;
    const h = ('' + date.getHours()).length === 1 ? '0' + date.getHours() : date.getHours();
    const mi = ('' + date.getMinutes()).length === 1 ? '0' + date.getMinutes() : date.getMinutes();

    return `${m}/${d} ${h}:${mi}`;
  }

  renderHead() {

    if (this.state.isFinal) {
      return (
        <div className={styles.listHead}>
          <div>排名</div>
          <div>姓名</div>
          <div>成绩</div>
          <div style={{ flex: 1.5 }}>城市</div>
          <div>奖品</div>
        </div>
      );
    }

    if (this.state.page === 'city') {
      return (
        <div className={styles.listHead}>
          <div>排名</div>
          <div>城市</div>
          <div>总人数</div>
          <div>总成绩</div>
          <div>总决赛</div>
        </div>
      );
    }
    return (
      <div className={styles.listHead}>
        <div>排名</div>
        <div>姓名</div>
        <div>成绩</div>
        <div style={{ flex: 1.5 }}>时间</div>
        <div>奖品</div>
      </div>
    );
  }

  renderRow(row, index) {

    if (this.state.isFinal) {
      return (
        <div className={styles.row} key={index}>
          <div>{row.rank_city}</div>
          <div className={styles.nickname}>{row.wx_nickname}</div>
          <div>{row.final_max_score}</div>
          <div style={{ flex: 1.5 }}>{row.xes_area_name}</div>
          <div>
            <div className={styles.prizeImg} style={row.prize_url ? { backgroundImage: `url(http://cdn-721008270.mtmss.com/${row.prize_url})` } : {}}></div>
          </div>
        </div>
      );
    }


    if (this.state.page === 'city') {
      return (
        <div className={styles.row} key={index}>
          <div>{index + 1}</div>
          <div>{row.xes_area_name}</div>
          <div>{row.people_num}</div>
          <div>{Math.floor(row.avg_score)}</div>
          <div>{index + 1 <= 3 ? '是' : '否'}</div>
        </div>
      );
    }

    return (
      <div className={styles.row} key={index}>
        <div>{this.state.page === 'country' ? row.rank_country : row.rank_city}</div>
        <div className={styles.nickname}>{row.wx_nickname}</div>
        <div>{row.max_score}</div>
        <div style={{ flex: 1.5 }}>{this.timeFormater(row.last_play_time)}</div>
        <div>
          <div className={styles.prizeImg} style={row.prize_url ? { backgroundImage: `url(http://cdn-721008270.mtmss.com/${row.prize_url})` } : {}}></div>
        </div>
      </div>
    );
  }

  renderTopButtons() {
    if (this.state.isFinal) {
      return null;
    }
    return (
      <div className={styles.topButtons}>
        {this.state.page !== 'person' && <div onClick={async () => {
          if (!this.state.user.province) {
            alert('暂无排名，快去参加比赛吧');
            return;
          }
          this.setState({ listIsLoading: true, hasMore: true, list: [], offset: 0 });
          const result = await this.getPersonRankList(0);
          this.setState({
            listIsLoading: false,
            offset: this.state.offset + this.limit,

            page: 'person',
            list: result.data,
            title: '我的城市排名'
          });
        }}>我的城市排名</div> || null}
        {this.state.page !== 'country' && <div onClick={async () => {
          this.setState({ listIsLoading: true, hasMore: true, list: [], offset: 0 });
          const result = await this.getCountryRankList(0);
          this.setState({
            listIsLoading: false,
            offset: this.state.offset + this.limit,

            page: 'country',
            list: result.data,
            title: '全国总排名'
          });
        }}>全国总排名</div> || null}
        {this.state.page !== 'city' && <div onClick={async () => {
          this.setState({ listIsLoading: true, hasMore: true, list: [], offset: 0 });
          const result = await this.getRankListBetweenCity(0);
          this.setState({
            listIsLoading: false,
            offset: this.state.offset + this.limit,

            page: 'city',
            list: result.data,
            title: '城市间排名'
          });
        }}>城市间排名</div> || null}
      </div>
    );
  }

  renderFooter() {
    return (
      <div className={styles.footer}>
        <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center', backgroundColor: '#4A90E2', width: '6.18rem' }}>
          <div style={{ fontSize: '0.42rem' }}>{this.state.user.nickname}</div>
          <div style={{ fontSize: '0.37rem' }}>{this.state.user.province || '暂无'}</div>
          <div style={{ fontSize: '0.37rem' }}>排名: {(this.state.isFinal ? this.state.user.final_rank : this.state.user.rank) || '暂无'}</div>
        </div>
        <button style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#EF6D00', border: 'none', outline: 'none', fontSize: '0.42rem', color: '#fff' }}
          onClick={() => {
            if (this.state.isFinal) {
              alert('总擂台争霸赛已结束');
              return;
            }
            location.href = '/game';
          }}>开始活动</button>
      </div>
    );
  }

  elementInfiniteLoad() {
    return <div key={'loading'} className={styles.listLoading}>
      加载中...
    </div>;
  }

  render() {

    return (
      <DocumentTitle title={this.state.title}>
        <div>
          {this.renderFooter()}
          {this.renderTopButtons()}
          {
            this.state.page === 'city'
              ? <div className={styles.rankImage}></div>
              : null
          }
          <div
            className={styles.rank}
          >
            {this.renderHead()}
            <InfiniteScroll
              pageStart={0}
              loadMore={this.handleOnBottom}
              hasMore={this.state.hasMore}
              loader={this.elementInfiniteLoad()}
              threshold={0}
            >
              <div className={styles.rows}>
                {this.state.list.map((item, index) => this.renderRow(item, index))}
              </div>
            </InfiniteScroll>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}