const mysql = require('mysql');

const dbConfig = require('../config/db.config');

const pool = mysql.createPool(dbConfig);

module.exports = pool;