const ERROR_CODE = {
  // 服务器相关
  3000: '服务器错误',

  // 数据库相关
  4000: '数据库执行失败',

  // 微信相关
  5000: '无效的授权code',
  5001: '获取微信用户信息错误',
  5002: 'app access_token获取错误',

  //用户相关
  6000: '用户不存在',
  6001: '学而思用户信息获取错误',
  6002: 'token不存在',
  6003: 'token无效',

  //其它
  7000: '缺少参数',

  // 总决赛列表
  8000: '填入总决赛列表失败',
  8001: '清空总决赛列表失败',
  8002: '刷新排行榜失败'
};

function errorCreator(code, error) {

  const eObj = {
    msg: ERROR_CODE[code] || '',
    code,
  };

  // TODO 判断环境
  if (error) {
    eObj.info = error;
  }
  

  return eObj;
}

function dataCreator(result) {
  return {
    data: result
  };
}

module.exports = {
  ErrorCreator: errorCreator,
  DataCreator: dataCreator,
};