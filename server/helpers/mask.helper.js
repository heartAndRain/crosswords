const crypto = require('crypto');

function getMaskResult(resolution) {
  const cache = {};
  return resolution.map(item => ({
    ...item,
    words: item.words.map(w => {
      if (!cache[w]) {
        const md5Word = crypto.createHash('md5').update(w).digest('base64');
        cache[w] = md5Word;
        return md5Word;
      }
      return cache[w];
    })
  }));
}

function checkMaskWordEquelWord(maskWord, word) {
  return crypto.createHash('md5').update(word).digest('base64') === maskWord;
}

module.exports = {
  getMaskResult,
  checkMaskWordEquelWord,
};