const redisConfig = require('../config/redis.config');
const Redis = require('ioredis');
const client = new Redis(redisConfig);

module.exports = client;
