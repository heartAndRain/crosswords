const isProd = process.env.NODE_ENV === 'production';
const isDev = process.env.NODE_ENV === 'dev';
const isLocal = !isProd && !isDev;

let config = {};

if (isLocal) {
  config = {
    host: '127.0.0.1',
    port : 6379,
  };
} else if (isDev) {
  config = {
    host: '172.16.61.247',
    port : 6379,
  };
} else if (isProd) {
  config = {
    host: 'r-2zecc4c54bf599a4.redis.rds.aliyuncs.com',
    port : 6379,
    password: 'nuxZz4Ykvsfsi7eq',
  };
}


module.exports = config;