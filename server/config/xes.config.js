const xesEnv = process.env.XESAPI_ENV;
const env = process.env.NODE_ENV;

const isProd = env === 'production';
const isDev = env === 'dev';
const isXesProd = xesEnv === 'production';

const testBaseUrl = 'http://ceshi.passport.haibian.com';
const prodBaseUrl = 'http://zxpassport.speiyou.com';

const baseUrl = isXesProd ? prodBaseUrl : testBaseUrl;
const orderBaseUrl = isXesProd ? 'http://order.speiyou.com' : 'http://ceshi.order.haibian.com';

const redirectUri = isProd ? 'http://crossword.haibian.com/game' : (isDev ? 'http://panziyi.net/game' : 'http://127.0.0.1:4000/game');

const course = isXesProd ? [
  '0101-RZv8ZCHg7wroA9Zp6Pd6vK',
  '0101-4Xoc1Q5TJDkrgej9adTUHY',
  '0101-3RZU9jAeb5q9DsrU6uXmb2',
  '0101-TR1oSQWXTvvvheW6gjATBS',
] : ['0519-9q7xLfUpBA7T356qjWWUDt','0519-VUfFtmCFYJyyMwwAyCtFKF'];


module.exports = {
  getLoginUri: () => `${baseUrl}/010/login?product=ss&terminal=2&redirect_uri=${encodeURI(redirectUri)}`,
  getUserInfoUri: ex_token => `${baseUrl}/getByToken?ex_token=${ex_token}`,
  getisPurchasedUri: (stuId, areaCode) => `${orderBaseUrl}/isCoursePurchasedByUser?studentId=${stuId}&area=${areaCode}&courseId=${JSON.stringify(course)}`,
};