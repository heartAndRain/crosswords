const env = process.env.NODE_ENV;
const isProd = env === 'production';
const isDev = env === 'dev';
const isLocal = !isProd && !isDev;

let appId = '';
let secret = '';
let redirect_uri = '';

if (isProd) {
  appId = 'wx63322d4552e9e714';
  secret = '93861ac951730ff108c60fd1f1ff0c61';
  redirect_uri = 'http://crossword.haibian.com';
} else if(isDev) {
  appId = 'wx73c1f415ac3a5169';
  secret = '3619a386f6029b5f464cc5a8195f5937';
  redirect_uri = 'http://panziyi.net';
} else {
  appId = 'wx73c1f415ac3a5169';
  secret = '3619a386f6029b5f464cc5a8195f5937';
  redirect_uri = 'http://127.0.0.1:4000';
}

const response_type = 'code';
const scope = 'snsapi_userinfo';
const state = 'common';

module.exports = {
  getCodeUri: () => `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appId}&redirect_uri=${redirect_uri}&response_type=${response_type}&scope=${scope}&state=${state}#wechat_redirect`,
  getAccessTokenUri: code => `https://api.weixin.qq.com/sns/oauth2/access_token?appid=${appId}&secret=${secret}&code=${code}&grant_type=authorization_code`,
  checkAccessTokenUri: (accessToken, openId) => `https://api.weixin.qq.com/sns/auth?access_token=${accessToken}&openid=${openId}`, 
  refreshTokenUri: refreshToken => `https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=${appId}&grant_type=refresh_token&refresh_token=${refreshToken}`,
  getUserInfoUri: (access_token, openid) => `https://api.weixin.qq.com/sns/userinfo?access_token=${access_token}&openid=${openid}&lang=zh_CN`,
  getAppAccessTokenUri: () => `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appId}&secret=${secret}`,
  getJsApiTicketUri: appAccessToken => `https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=${appAccessToken}&type=jsapi`,
};
