const pool = require('../helpers/db.helper');

class Resolution {
  async getRandomResolution(randomId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM resolutions WHERE id = ?';

        connection.query(sql,[randomId],(err, result) => {
          if (err) {
            return reject(err);
          }
          const { id, resolution } = result[0];
          resolve({id, resolution: JSON.parse(resolution)});
          connection.release();
        });
      });
    });
  }
  async getRandomTryResolution(randomId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM try_resolutions WHERE id = ?';

        connection.query(sql, [randomId], (err, result) => {
          if (err) {
            return reject(err);
          }
          const { id, resolution } = result[0];
          resolve({id, resolution: JSON.parse(resolution)});
          connection.release();
        });
      });
    });
  }

  async getAllWords() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM words';

        connection.query(sql, (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async getAllTryWords() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM try_words';

        connection.query(sql, (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }
}

module.exports = new Resolution();


