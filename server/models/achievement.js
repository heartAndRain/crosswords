const pool = require('../helpers/db.helper');

class Achievement {
  async submitScore(openId, score) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          connection.release();
          return reject(err);
        }

        const getCurrentScoreSql = 'SELECT max_score FROM achievement WHERE wx_openId = ? ';
        const insertScoreSql = 'INSERT INTO achievement SET wx_openId = ?, max_score = ?, last_play_time = ?';
        const updateScoreSql = 'UPDATE achievement SET max_score = ?, last_play_time = ? where wx_openId = ?';
        const updatePlayTimeSql = 'UPDATE achievement SET last_play_time = ? where wx_openId = ?';

        const playTime = Date.now();

        connection.query(getCurrentScoreSql, [openId], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          
          if (!result[0]) {
            connection.query(insertScoreSql, [openId, score, playTime], (err, result) => {
              if (err) {
                connection.release();
                return reject(err);
              }
              resolve();
              connection.release();
            });
            return;
          }

          const { max_score } = result[0];
          if (score > max_score) {
            connection.query(updateScoreSql, [score, playTime, openId], (err, result) => {
              if (err) {
                connection.release();
                return reject(err);
              }
              resolve();
              connection.release();
            });
          } else {
            connection.query(updatePlayTimeSql, [playTime, openId], (err) => {
              if (err) {
                connection.release();
                return reject(err);
              }
              resolve();
              connection.release();
            });
          }
        });
      });
    });
  }

  async submitFinalScore(openId, score) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }

        const getCurrentScoreSql = 'SELECT final_max_score FROM final_achievement WHERE wx_openId = ? ';
        const updateScoreSql = 'UPDATE final_achievement SET final_max_score = ?, last_play_time = ? where wx_openId = ?';
        const updatePlayTimeSql = 'UPDATE final_achievement SET last_play_time = ? where wx_openId = ?';

        const playTime = Date.now();

        connection.query(getCurrentScoreSql, [openId], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          
          if (!result[0]) {
            return;
          }

          const { final_max_score } = result[0];
          if (score > final_max_score) {
            connection.query(updateScoreSql, [score, playTime, openId], (err, result) => {
              if (err) {
                connection.release();
                return reject(err);
              }
              resolve();
              connection.release();
            });
          } else {
            connection.query(updatePlayTimeSql, [playTime, openId], (err) => {
              if (err) {
                connection.release();
                return reject(err);
              }
              resolve();
              connection.release();
            });
          }
        });


      });
    });
  }

  async getOriginCountryRankList() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'select user.wx_openId, user.wx_nickname, user.xes_area_code, user.xes_area_name, achievement.max_score, achievement.last_play_time from user,achievement where user.wx_openId = achievement.wx_openId ORDER BY achievement.max_score DESC,achievement.last_play_time';
        connection.query(sql, (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async fillCountryRankList(list) {

    return new Promise((resolve, reject) => {

      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }

        const query = sql => (new Promise((rs, rj) => {
          connection.query(sql, (err, qResult) => {
            if (err) {
              connection.release();
              return rj(err);
            }
            rs(qResult);
          });
        }));

        connection.beginTransaction(async (err) => {
          if (err) {
            connection.release();
            return reject(err);
          }

          // delete old
          await query('DELETE from country_rank');

          for (const item of list) {
            const { wx_openId, wx_nickname, xes_area_code, xes_area_name, max_score, last_play_time, rank_country } = item;
            const sql = `INSERT INTO country_rank(wx_openId, wx_nickname, xes_area_code, xes_area_name, max_score, last_play_time, rank_country) VALUES('${wx_openId}', '${wx_nickname}', '${xes_area_code}', '${xes_area_name}', '${max_score}', '${last_play_time}', '${rank_country}')`;
            try {
              await query(sql);
            } catch (error) {
              connection.rollback(err => reject(err));
              reject(error);
            }
          }
          
          connection.commit(err => reject(err));
          resolve();
          connection.release();
        });
      });
    });
  }

  async getCountryRankList(limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT * FROM country_rank ORDER BY rank_country LIMIT ? OFFSET ?';
        connection.query(sql.replace(/\n/g, ''), [limit, offset], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          connection.query('SELECT COUNT(*) as total FROM country_rank', (err, rows) => {
            if (err) {
              connection.release();
              return reject(err);
            }
            resolve({result, total: rows[0].total});
            connection.release();
          });
        });
      });
    });
  }

  async getCityRankNumberByOpenId(city, openId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT s1.wx_openId,s1.wx_nickname,s1.max_score,s1.last_play_time,s1.rank_country,COUNT(DISTINCT s2.max_score) AS rank_city FROM country_rank s1 JOIN country_rank s2 ON (s1.max_score<=s2.max_score) WHERE s1.xes_area_code= ? AND s2.xes_area_code= ? AND s1.wx_openId = ? GROUP BY s1.wx_openId ORDER BY rank_city';
        connection.query(sql, [city, city, openId], (err, result) => {
          if (err) {
            connection.release();
            return reject(err || 'can not get this user city');
          }
          resolve(result[0]);
          connection.release();
        });
      });
    });
  }

  async getPrizeList() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT * FROM prize';
        connection.query(sql, (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async getRankListBetweenCity(limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = `SELECT user.xes_area_code, user.xes_area_name, COUNT(user.xes_area_code) as people_num, AVG(achievement.max_score) as avg_score 
        FROM user,achievement 
        WHERE user.wx_openId = achievement.wx_openId 
        GROUP BY user.xes_area_code 
        ORDER BY avg_score DESC
        LIMIT ?
        OFFSET ?
        `;
        connection.query(sql.replace(/\n/g, ''), [limit, offset], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async getPersonRankList(openId, limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT * FROM country_rank WHERE rank >= (SELECT rank FROM country_rank WHERE wx_openId = ? )';
        connection.query(sql, [openId], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }


  async getCityRankList(cityCode, limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT wx_openId, wx_nickname, max_score, last_play_time, rank_country FROM country_rank WHERE xes_area_code = ? ORDER BY rank_country LIMIT ? OFFSET ?';
        connection.query(sql,[cityCode, limit, offset], (err,result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          
          const totalSql = 'SELECT count(*) FROM country_rank WHERE xes_area_code = ? ORDER BY rank_country';
          
          connection.query(totalSql, [cityCode], (err, rows) => {
            if (err) {
              connection.release();
              return reject(err);
            }
            resolve({result, total: rows[0].total});
            connection.release();
          });
          
        });
      });
    });
  }

  

  async searchRankList(word, limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM country_rank where wx_nickname like CONCAT("%", ?, "%") LIMIT ? OFFSET ?';
        connection.query(sql,[word, limit, offset], (err,result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          
          const totalSql = 'SELECT COUNT(*) as total FROM country_rank where wx_nickname like CONCAT("%", ?, "%")';
          
          connection.query(totalSql, [word], (err, rows) => {
            if (err) {
              connection.release();
              return reject(err);
            }
            resolve({result, total: rows[0].total});
            connection.release();
          });
          
        });
      });
    });
  }

  async getFinalsList(limit, offset) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT * FROM final_achievement ORDER BY final_max_score DESC LIMIT ? OFFSET ?';
        connection.query(sql.replace(/\n/g, ''), [limit, offset], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          connection.query('SELECT COUNT(*) as total FROM final_achievement', (err, rows) => {
            if (err) {
              connection.release();
              return reject(err);
            }
            resolve({result, total: rows[0].total});
            connection.release();
          });
        });
      });
    });
  }

  async getAvgCityInFinalsList() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT xes_area_code,xes_area_name,avg(final_max_score) as avg_score FROM final_achievement GROUP BY xes_area_code ORDER BY avg_score DESC';
        connection.query(sql, (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async fillFinalList(allUsers) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const query = sql => (new Promise((rs, rj) => {
          connection.query(sql, (err, qResult) => {
            if (err) {
              connection.release();
              return rj(err);
            }
            rs(qResult);
          });
        }));

        connection.beginTransaction(async (err) => {
          if (err) {
            connection.release();
            return reject(err);
          }

          // delete old
          await query('DELETE from final_achievement');
          for (const user of allUsers) {
            
            const sql = `INSERT INTO final_achievement(wx_openId, wx_nickname, xes_area_code, xes_area_name, max_score, last_play_time) VALUES('${user.wx_openId}', '${user.wx_nickname}','${user.xes_area_code}', '${user.xes_area_name}', '${user.max_score}', '${user.last_play_time}')`;
            try {
              await query(sql);
            } catch (error) {
              connection.rollback(err => reject(err));
              reject(error);
            }
          }

          connection.commit(err => reject(err));
          resolve();
          connection.release();
        });
      });
    });
  }

  // 是否进入总决赛
  async isEnterFinalList(openId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          connection.release();
          return reject(err);
        }
        const sql = 'SELECT * FROM final_achievement WHERE wx_openId = ?';
        connection.query(sql,[openId], (err, result) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(result.length === 0 ? false : true);
          connection.release();
        });
      });
    });
  }
}

module.exports = new Achievement();


