const pool = require('../helpers/db.helper');

class Info {
  async getInfo() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  'select * from info where id = 1';
        connection.query(sql, (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(rows[0]);
          connection.release();
        });

      });
    });
  }
  async updateGameTime(time) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  'update info set game_time = ? where id = 1';
        connection.query(sql, [time], (err) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }

  async updateFinalsDate(date) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  'update info set finals_time = ? where id = 1';
        connection.query(sql, [date], (err) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }

  async updateGameRules(text) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          return reject(err);
        }
        const sql =  'update info set game_rules = ? where id = 1';
        connection.query(sql, [text], (err) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }
}
module.exports = new Info();