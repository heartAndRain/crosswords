const pool = require('../helpers/db.helper');

class User {

  // 会暴露access token,只能内部使用
  async getWxUserInfo(openId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT wx_openId,wx_nickname,wx_access_token,wx_refresh_token FROM user WHERE `wx_openId` = ?';
        
        connection.query(sql, [openId], (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve(result[0]);
          connection.release();
        });
      });
    });
  }
  async getUserInfoByOpenId(openId) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT wx_nickname,xes_uid,xes_stu_id,xes_area_code,xes_area_name,xes_is_purchased FROM user WHERE `wx_openId` = ?';
        
        connection.query(sql, [openId], (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve(result[0]);
          connection.release();
        });
      });
    });
  }
  async addXesUserInfoByOpenId(openId, xesInfo) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const { xes_uid, xes_stu_id, xes_area_code, xes_area_name, xes_is_purchased } = xesInfo;

        if (!xes_uid || !xes_stu_id || !xes_area_code || !xes_area_name) {
          return reject(`参数错误,${xes_uid},${xes_stu_id},${xes_area_code},${xes_area_name}`);
        }

        const sql = 'UPDATE user SET xes_uid = ?, xes_stu_id = ?, xes_area_code = ?, xes_area_name = ?, xes_is_purchased = ? WHERE `wx_openId` = ?';
        
        connection.query(sql, [xes_uid, xes_stu_id, xes_area_code, xes_area_name, xes_is_purchased, openId], (err, result) => {
          if (err) {
            return reject(err);
          }
          resolve();
          connection.release();
        });
      });
    });
  }
  async addOrUpdateWxUserInfo(wxUserInfo) {
    return new Promise((resolve, reject) => {

      const { openid, access_token, refresh_token, nickname } = wxUserInfo;

      if (!openid || !access_token || !refresh_token) {
        return reject('微信用户信息插入错误');
      }

      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = `INSERT INTO user(wx_openId, wx_access_token, wx_refresh_token, wx_nickname) VALUES('${openid}', '${access_token}','${refresh_token}', '${nickname}') ON DUPLICATE KEY UPDATE wx_access_token = '${access_token}', wx_refresh_token = '${refresh_token}'`;
        connection.query(sql, err => {
          if (err) {
            return reject(err);
          }
          resolve();
          connection.release();
        });
      });
    });
  }

  async getAllCity() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT xes_area_name AS city_name,xes_area_code AS city_code FROM user GROUP BY xes_area_code';
        connection.query(sql, (err,result) => {
          if (err) {
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async getProvincePeople() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          return reject(err);
        }
        const sql = 'select city.province, count(*) as people_num FROM user,city WHERE user.xes_area_name != "" and user.xes_is_purchased != 0 and city.city LIKE CONCAT(user.xes_area_name, "%")  GROUP BY city.province';
        connection.query(sql, (err,result) => {
          if (err) {
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  async getPeopleByCity(areaCode) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          return reject(err);
        }
        const sql = 'SELECT user.wx_openId, user.wx_nickname, user.xes_area_code, user.xes_area_name, achievement.max_score, achievement.last_play_time FROM user,achievement WHERE xes_area_code = ? and user.wx_openId = achievement.wx_openId order by achievement.max_score DESC';
        connection.query(sql, [areaCode], (err,result) => {
          if (err) {
            return reject(err);
          }
          resolve(result);
          connection.release();
        });
      });
    });
  }

  // admin user
  async getAdminUserByUsername(username) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if (err) {
          return reject(err);
        }
        const sql = 'SELECT * FROM `admin_user` WHERE username = ?';
        connection.query(sql,[username], (err,result) => {
          if (err) {
            return reject(err);
          }
          resolve(result[0]);
          connection.release();
        });
      });
    });
  }
}

module.exports = new User();


