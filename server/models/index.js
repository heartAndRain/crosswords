const resolution = require('./resolution');
const user = require('./user');
const achievement = require('./achievement');
const prize = require('./prize');
const info = require('./info');
const app = require('./app');

module.exports = {
  Resolution: resolution,
  User: user,
  Achievement: achievement,
  Prize: prize,
  Info: info,
  App: app,
};