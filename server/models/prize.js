const pool = require('../helpers/db.helper');

class Prize {
  async getPrizeInfo() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql = 'SELECT * FROM prize';
        connection.query(sql, (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve(rows);
          connection.release();
        });

      });
    });
  }

  async addSpecPrize(prize_name, prize_url, rank_num) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  `INSERT INTO prize(prize_name, prize_url, type, rank_num) VALUES('${prize_name}', '${prize_url}','spec', '${rank_num}')`;
        connection.query(sql, (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }

  async updateSpecPrize(prize_name, prize_url, rank_num) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  'UPDATE prize SET prize_name = ?, prize_url = ?  WHERE rank_num = ? AND type = "spec"';
        connection.query(sql, [prize_name, prize_url, rank_num], (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }

  async addSpecNumPrize(prize_name, prize_url, rank_num) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  `INSERT INTO prize(prize_name, prize_url, type, rank_num) VALUES('${prize_name}', '${prize_url}','specn', '${rank_num}')`;
        connection.query(sql, (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }

  async updateSpecNumPrize(prize_name, prize_url, rank_num) {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {

        if (err) {
          return reject(err);
        }

        const sql =  'UPDATE prize SET prize_name = ?, prize_url = ?, rank_num = ? WHERE type = "specn"';
        connection.query(sql, [prize_name, prize_url, rank_num], (err, rows) => {
          if (err) {
            connection.release();
            return reject(err);
          }
          resolve();
          connection.release();
        });

      });
    });
  }
}

module.exports = new Prize();