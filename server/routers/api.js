/* eslint no-inner-declarations: off */
const crypto = require('crypto');
const express = require('express');
const router = express.Router();
const request = require('request-promise-native');

const { ErrorCreator, DataCreator } = require('../helpers/response.helper');
const { getMaskResult, checkMaskWordEquelWord } = require('../helpers/mask.helper');
const { getAppAccessTokenUri, getJsApiTicketUri } = require('../config/wechat.config');
const Model = require('../models');

router.get('/', (req, res) => {
  res.sendStatus(404);
});

router.get('/getSignature', async (req, res) => {
  const { nonceStr, timestamp, url } = req.query;
  if (!nonceStr || !timestamp || !url) {
    return res.sendStatus(500);
  }
  try {

    let jsApiTicket = await Model.App.getJsApiTicket();

    if (!jsApiTicket) {
      let accessToken = await Model.App.getAppAccessToken();

      if (!accessToken) {
        const { access_token, expires_in, errcode, errmsg} = JSON.parse(await request(getAppAccessTokenUri()));
        if (errcode && errcode !== 0) {
          throw new Error(`${errcode}:${errmsg}`);
        }
        await Model.App.setAppAccessToken(access_token, expires_in - 10); // redis提前10s过期
        accessToken = access_token;
      }
      const {ticket, expires_in, errcode, errmsg} = JSON.parse(await request(getJsApiTicketUri(accessToken)));

      if (errcode !== 0) {
        throw new Error(`${errcode}:${errmsg}`);
      }
      await Model.App.setJsApiTicket(ticket, expires_in - 10);
      jsApiTicket = ticket;
    }

    const str = `jsapi_ticket=${jsApiTicket}&noncestr=${nonceStr}&timestamp=${+timestamp}&url=${url}`;
    console.log(jsApiTicket, nonceStr, +timestamp, url);
    const signature = crypto.createHash('sha1').update(str).digest('hex');

    res.json(DataCreator({signature}));
  } catch (error) {
    res.status(500).json(ErrorCreator(3000, `${error}`));
  }
});

// resolution 相关
router.get('/getResolution', async (req, res) => {

  const max = 19239, min = 15232;
  let randomId = +(Math.random() * (max - min) + min).toFixed(0);
  
  while(req.session.lastRandomId === randomId) {
    randomId = +(Math.random() * (max - min) + min).toFixed(0);
  }
  req.session.lastRandomId = randomId;

  try {
    const result = await Model.Resolution.getRandomResolution(randomId);

    res.json(DataCreator({ id: result.id, resolution: getMaskResult(result.resolution)}));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getTryResolution', async (req, res) => {

  const max = 2259, min = 2107;
  let randomId = +(Math.random() * (max - min) + min).toFixed(0);

  while(req.session.lastTryRandomId === randomId) {
    randomId = +(Math.random() * (max - min) + min).toFixed(0);
  }
  req.session.lastTryRandomId = randomId;

  try {
    const result = await Model.Resolution.getRandomTryResolution(randomId);

    res.json(DataCreator({ id: result.id, resolution: getMaskResult(result.resolution)}));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getAllWords', async (req, res) => {
  try {
    const result = await Model.Resolution.getAllWords();

    res.json(DataCreator(result));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getAllTryWords', async (req, res) => {
  try {
    const result = await Model.Resolution.getAllTryWords();

    res.json(DataCreator(result));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

// user 相关
router.get('/getUserInfo', async (req, res) => {

  const { wx_openId: openId } = req.wxUser;

  try {

    const userInfo = await Model.User.getUserInfoByOpenId(openId);

    if (!userInfo) {
      return res.status(400).json(ErrorCreator(6000));
    }

    // 是否进入总决赛
    const isEnterFinal = await Model.Achievement.isEnterFinalList(openId);
    let final_rank = null;
    if (isEnterFinal) {
      const { result: finalList } = await Model.Achievement.getFinalsList(10000, 0);
      
      const index = finalList.findIndex(i => i.wx_openId === openId);
      final_rank = index !== -1 ? index + 1 : null;
    }

    const info = {
      nickname: userInfo.wx_nickname,
      province: userInfo.xes_area_name,
      area_code: userInfo.xes_area_code,
      is_enter_final: isEnterFinal,
      final_rank
    };
    

    if (!userInfo.xes_area_code) {
      info.rank = null;
      return res.json(DataCreator(info));
    }

    const rankResult = await Model.Achievement.getCityRankList(userInfo.xes_area_code, 10000000, 0);
  
    const rank = rankResult.result.findIndex(i => i.wx_openId === openId);
    info.rank = rank === -1 ? null : rank + 1;

    res.json(DataCreator(info));
  } catch (e) {
    return res.status(500).json(ErrorCreator(5001, e));
  }
});

router.get('/getProvincePeople', async (req, res) => {
  try {
    const result = await Model.User.getProvincePeople();
    res.json(DataCreator(result));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});


// 游戏 相关
router.post('/submitSolution', async (req, res) => {
  const { resolveWord, maskWord } = req.body;
  req.session.gameScore = req.session.gameScore || 0;

  if (checkMaskWordEquelWord(maskWord, resolveWord)) {

    req.session.gameScore += 10;

    res.json(DataCreator({ correct: true, currentScore: req.session.gameScore, }));
    return;
  }

  res.json(DataCreator({ correct: false, currentScore: req.session.gameScore, }));
});


router.post('/gameStart', async (req, res) => {
  req.session.gameScore = 0;
  res.json(DataCreator({ success: true }));
});

router.post('/gameFinish', async (req, res) => {
  const { isTry, isFinal } = req.body;
  const score = req.session.gameScore || 0;

  // 清零
  req.session.gameScore = 0;

  const { wx_openId } = req.wxUser;

  if (isFinal) {
    // try {
    //   await Model.Achievement.submitFinalScore(wx_openId, score);
    // } catch (e) {
    //   return res.status(500).json(ErrorCreator(4000, e));
    // }
    // res.json(DataCreator({ success: { score, openId: wx_openId } }));
    // return;
  }


  if (typeof isTry !== 'undefined' && !isTry) {
    try {
      await Model.Achievement.submitScore(wx_openId, score);
    } catch (e) {
      return res.status(500).json(ErrorCreator(4000, e));
    }
  }

  res.json(DataCreator({ success: { score, openId: wx_openId } }));

});

// 排名相关

const getPrizeUrl = (specRankList, specNumberPrize, comPrize) => {

  return item => {
    if (specRankList.length) {
      const spec = specRankList.find(i => i.rank_num === item.rank_country);
      if (spec) {
        return spec.prize_url;
      }
    }
    if (specNumberPrize) {
      if (specNumberPrize.rank_num === +`${item.rank_country}`.slice(-1)) {
        return specNumberPrize.prize_url;
      }
    }
    if (comPrize) {
      return comPrize.prize_url;
    }
    return '';
  };
};

// 全国排名
router.get('/getCountryRankList', async (req, res) => {
  let { limit, offset } = req.query;

  limit = limit || 20;
  offset = offset || 0;

  try {
    const { result: rankList } = await Model.Achievement.getCountryRankList(+limit, +offset);

    const prizeList = await Model.Achievement.getPrizeList();

    // 特殊奖品范围数组，比如1-20
    const specRankList = prizeList.filter(i => i.type === 'spec');
    // 普通奖品
    const comPrize = prizeList.find(i => i.type === 'com');
    // 幸运数字奖品
    const specNumberPrize = prizeList.find(i => i.type === 'specn');

    const list = rankList.map(item => ({
      ...item,
      prize_url: getPrizeUrl(specRankList, specNumberPrize, comPrize)(item),
    }));


    res.json(DataCreator(list));
  } catch (error) {
    return res.status(500).json(ErrorCreator(4000, error));
  }

});

// 城市间排名
router.get('/getRankListBetweenCity', async (req, res) => {
  let { limit, offset } = req.query;

  limit = limit || 20;
  offset = offset || 0;

  try {
    const rankList = await Model.Achievement.getRankListBetweenCity(+limit, +offset);

    res.json(DataCreator(rankList));

  } catch (error) {
    return res.status(500).json(ErrorCreator(4000, error));
  }
});

// 城市内部排名
router.get('/getCityRankList', async (req, res) => {
  let { limit, offset, city } = req.query;

  if (!city) return res.status(400).json(ErrorCreator(7000));

  limit = limit || 20;
  offset = offset || 0;

  try {
    const { result } = await Model.Achievement.getCityRankList(city, +limit, +offset);

    const rankList = result.map((i, index) => ({...i, rank_city: +offset + index + 1}));

    const prizeList = await Model.Achievement.getPrizeList();

    // 特殊奖品范围数组，比如1-20
    const specRankList = prizeList.filter(i => i.type === 'spec');
    // 普通奖品
    const comPrize = prizeList.find(i => i.type === 'com');
    // 幸运数字奖品
    const specNumberPrize = prizeList.find(i => i.type === 'specn');

    const list = rankList.map(item => ({
      ...item,
      prize_url: getPrizeUrl(specRankList, specNumberPrize, comPrize)(item),
    }));

    res.json(DataCreator(list));

  } catch (error) {
    return res.status(500).json(ErrorCreator(4000, error));
  }
});

// 总决赛排名
router.get('/getFinalList', async (req, res) => {
  let { limit, offset } = req.query;

  limit = limit || 20;
  offset = offset || 0;

  try {
    const prizeList = await Model.Achievement.getPrizeList();
    const specRankPrizeMap = prizeList.filter(i => i.type === 'final-spec').reduce((obj, r) => {
      obj[r.rank_num] = r.prize_url;
      return obj;
    }, {});
    const cityResult = await Model.Achievement.getAvgCityInFinalsList(); // Array<{xes_area_code, xes_area_name, avg_score}>

    const cityPrizeMap = cityResult.reduce((obj, c, index) => {
      obj[c.xes_area_code] = specRankPrizeMap[index + 1];
      return obj;
    }, {});

    const { result: personResult } = await Model.Achievement.getFinalsList(+limit, +offset);
    const rankList = personResult.map((i, index) => ({...i, rank_city: +offset + index + 1, prize_url: cityPrizeMap[i.xes_area_code]}));

    res.json(DataCreator(rankList));

  } catch (error) {
    return res.status(500).json(ErrorCreator(4000, error));
  }
});

router.get('/getInfo', async (req, res) => {
  try {
    const info = await Model.Info.getInfo();
    res.json(DataCreator(info));
  } catch (e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

module.exports = router;