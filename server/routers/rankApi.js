const Model = require('../models');
const { ErrorCreator, DataCreator } = require('../helpers/response.helper');

module.exports = {
  rank: async (req, res) => {
    try {
      const originRankList = await Model.Achievement.getOriginCountryRankList();

      const rankList = originRankList.map((i, index) => ({...i, rank_country: index + 1}));

      await Model.Achievement.fillCountryRankList(rankList);

      res.json(DataCreator({ success: true }));
      
    } catch (e) {
      return res.status(500).json(ErrorCreator(8002, e));
    }
  }
};