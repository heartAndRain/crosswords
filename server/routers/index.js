const frontEndRouter = require('./frontend');
const apiRouter = require('./api');
const adminRouter = require('./adminApi');

module.exports = {
  frontEndRouter,
  apiRouter,
  adminRouter,
};