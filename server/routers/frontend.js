const express = require('express');
const router = express.Router();
const path = require('path');
const request = require('request-promise-native');

const Model = require('../models');
const xesConfig = require('../config/xes.config');
const { ErrorCreator } = require('../helpers/response.helper');

router.get(/^\/(index|game|try|board)?$/, async (req, res) => {

  // game权限限制
  if (/\/game\/?/.test(req.path)) {

    const { wx_openId } = req.wxUser;

    try {
      const userXesInfo = await Model.User.getUserInfoByOpenId(wx_openId);

      if (req.query.ex_token && !userXesInfo.xes_uid) {
        const resultInfo = JSON.parse(await request(xesConfig.getUserInfoUri(req.query.ex_token)));

        if (resultInfo.errcode !== 0) {
          return res.status(500).json(ErrorCreator(6001, JSON.stringify(resultInfo)));
        }

        const { uid, stu_id, area, city_name } = resultInfo.data;

        const isPurchasedInfo = JSON.parse(await request(xesConfig.getisPurchasedUri(stu_id, area)));

        if (isPurchasedInfo.data !== 'regist') {
          // 写库
          await Model.User.addXesUserInfoByOpenId(wx_openId, {
            xes_uid: uid,
            xes_stu_id: stu_id,
            xes_area_code: area,
            xes_area_name: city_name,
            xes_is_purchased: 0,
          });
          return res.redirect('http://h5.speiyou.com/zt/2018030514545477.html');
        }

        // 写库
        await Model.User.addXesUserInfoByOpenId(wx_openId, {
          xes_uid: uid,
          xes_stu_id: stu_id,
          xes_area_code: area,
          xes_area_name: city_name,
          xes_is_purchased: 1,
        });

      } else if (userXesInfo.xes_uid && userXesInfo.xes_stu_id && userXesInfo.xes_area_code) {
        // && (!userXesInfo.xes_is_purchased || userXesInfo.xes_is_purchased === 0))

        if (+userXesInfo.xes_is_purchased !== 1) {
          const isPurchasedInfo = JSON.parse(await request(xesConfig.getisPurchasedUri(userXesInfo.xes_stu_id, userXesInfo.xes_area_code)));
          if (isPurchasedInfo.data !== 'regist') {
            return res.redirect('http://h5.speiyou.com/zt/2018030514545477.html');
          } else {
            await Model.User.addXesUserInfoByOpenId(wx_openId, {
              xes_uid: userXesInfo.xes_uid,
              xes_stu_id: userXesInfo.xes_stu_id,
              xes_area_code: userXesInfo.xes_area_code,
              xes_area_name: userXesInfo.xes_area_name,
              xes_is_purchased: 1,
            });
          }
        }
      } else if (!userXesInfo.xes_uid) {
        return res.redirect(xesConfig.getLoginUri());
      }
    } catch (e) {
      return res.status(500).json(ErrorCreator(6001, JSON.stringify(e)));
    }

    res.sendFile(path.resolve(__dirname, '../../client/dist/index.html'));
  } else {
    res.sendFile(path.resolve(__dirname, '../../client/dist/index.html'));
  }

});

module.exports = router;