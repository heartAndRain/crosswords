const Model = require('../models');
const { ErrorCreator, DataCreator } = require('../helpers/response.helper');

function getFirstN(n, arr, field) {
  const tmpA = [];
  const result = [];
  let count = 0, index = 0;

  while (count < n && index < arr.length) {
    result.push(arr[index]);
    const tmp = field ? arr[index][field] : arr[index];
    if (tmpA.indexOf(tmp) === -1) {
      tmpA.push(tmp);
      count++;
    }
    index++;
  }

  return result;
}

module.exports = {
  fill: async (req, res) => {
    try {
      const dbResult = await Model.Achievement.getRankListBetweenCity(20, 0);

      const theFirstThree = getFirstN(3, dbResult, 'avg_score');
      let allUsers = [];
      for (const city of theFirstThree) {
        const users = await Model.User.getPeopleByCity(city.xes_area_code);
        const firstTenUsers = users.slice(0, 10);
        allUsers.push(...firstTenUsers);
      }

      allUsers.sort((a, b) => b.max_score - a.max_score);

      
      await Model.Achievement.fillFinalList(allUsers);

      res.send(DataCreator({ success: true }));
    } catch (error) {
      return res.status(500).json(ErrorCreator(8000, error));
    }

  }
};


