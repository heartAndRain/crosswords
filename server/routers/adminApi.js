const crypto = require('crypto');
const express = require('express');
const router = express.Router();
const path = require('path');
const multer  = require('multer');
const jwt = require('jsonwebtoken');
const mss = require('../helpers/mss.helper');

const adminSecretKey = require('../config/admin.secret.config');
const storage = multer.memoryStorage();
const upload = multer({ storage });
const { ErrorCreator, DataCreator } = require('../helpers/response.helper');

const Model = require('../models');

router.post('/login', upload.fields([]), async (req, res) => {

  const { username, password } = req.body;

  if (!username || !password) {
    return res.json(ErrorCreator(6000, '无效的用户名和密码'));
  }

  const user = await Model.User.getAdminUserByUsername(username);

  if (!user || user.password !== password) {
    return res.json(ErrorCreator(6000, '用户名或密码错误'));
  }

  const token = jwt.sign({ username}, adminSecretKey, { expiresIn: '7d'});
 
  res.json(DataCreator({ user: username, token }));
});

router.post('/checkToken', upload.fields([]), (req, res) => {
  const { token } = req.body;

  if(!token) {
    return res.status(500).json(ErrorCreator(6002));
  }

  jwt.verify(token ,adminSecretKey, (err, decoded) => {
    if (err) {
      return res.status(500).json(ErrorCreator(6003));
    }
    res.json({user: decoded.username});
  });
});

router.get('/getAllCity', async (req, res) => {
  try {
    const result = await Model.User.getAllCity();
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getCountryRank', async (req, res) => {
  let { limit, offset } = req.query;

  limit = limit || 20;
  offset = offset || 0;

  try {
    const result = await Model.Achievement.getCountryRankList(+limit, +offset);
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getCityRank', async (req, res) => {

  let { limit, offset, city } = req.query;

  if (!city) return res.status(400).json(ErrorCreator(7000));

  limit = limit || 20;
  offset = offset || 0;

  try {
    const result = await Model.Achievement.getCityRankList(city, +limit, +offset);
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});


router.get('/getFinalsList', async (req, res) => {

  let { limit, offset } = req.query;

  limit = limit || 20;
  offset = offset || 0;

  try {
    const result = await Model.Achievement.getFinalsList(+limit, +offset);
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/searchRankList', async (req, res) => {

  let { limit, offset, word } = req.query;

  if (!word) return res.status(400).json(ErrorCreator(7000));

  limit = limit || 20;
  offset = offset || 0;

  try {
    const result = await Model.Achievement.searchRankList(word, +limit, +offset);
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getPrizeInfo', async (req, res) => {
  try {
    const result = await Model.Prize.getPrizeInfo();
    res.json(DataCreator(result));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/upload', upload.single('image'), async (req, res) => {

  const file = req.file;
  const filename = crypto.createHash('md5').update(file.buffer).digest('hex');
  try {
    const upRes = await mss.putObject(filename, file.buffer);
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }

  res.json(DataCreator({message: 'ok', name: filename}));
});

router.post('/addSpecPrize',upload.fields([]), async (req, res) => {
  const {prize_name, prize_url, rank_num } = req.body;

  if (!prize_name || !prize_url || !rank_num) return res.status(500).json(ErrorCreator(4000, 'lack of params'));

  try {
    await Model.Prize.addSpecPrize(prize_name, prize_url, rank_num);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/updateSpecPrize',upload.fields([]), async (req, res) => {
  const {prize_name, prize_url, rank_num } = req.body;

  if (!prize_name || !prize_url || !rank_num) return res.status(500).json(ErrorCreator(4000, 'lack of params'));

  try {
    await Model.Prize.updateSpecPrize(prize_name, prize_url, rank_num);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/addSpecNumPrize',upload.fields([]), async (req, res) => {
  const {prize_name, prize_url, rank_num } = req.body;

  if (!prize_name || !prize_url || !rank_num) return res.status(500).json(ErrorCreator(4000, 'lack of params'));

  try {
    await Model.Prize.addSpecNumPrize(prize_name, prize_url, rank_num);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/updateSpecNumPrize',upload.fields([]), async (req, res) => {
  const {prize_name, prize_url, rank_num } = req.body;

  if (!prize_name || !prize_url || !rank_num) return res.status(500).json(ErrorCreator(4000, 'lack of params'));

  try {
    await Model.Prize.updateSpecNumPrize(prize_name, prize_url, rank_num);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.get('/getInfo', async (req, res) => {
  try {
    const info = await Model.Info.getInfo();
    res.json(DataCreator(info));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});


router.post('/updateGameTime', upload.fields([]), async (req, res) => {
  const { game_time } = req.body;
  try {
    await Model.Info.updateGameTime(game_time);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/updateFinalsTime', upload.fields([]), async (req, res) => {
  const { finals_time } = req.body;
  try {
    await Model.Info.updateFinalsDate(finals_time);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

router.post('/updateGameRules', upload.fields([]), async (req, res) => {
  const { game_rules } = req.body;
  try {
    await Model.Info.updateGameRules(game_rules);
    res.json(DataCreator({message: 'ok'}));
  } catch(e) {
    return res.status(500).json(ErrorCreator(4000, e));
  }
});

module.exports = router;