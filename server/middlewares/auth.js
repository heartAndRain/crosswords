const request = require('request-promise-native');
const wechatConfig = require('../config/wechat.config');
const { User } = require('../models');
const { ErrorCreator } = require('../helpers/response.helper');

module.exports = async (req, res, next) => {
  const openId = req.cookies.openId;
  const { code, state } = req.query;

  // 静态目录不授权
  if (/^(?:\/(static|admin)\/?)/.test(req.path)) {
    return next();
  }


  // openId注意大小写
  if (openId) {

    // TODO 查缓存不查库
    let user = await User.getWxUserInfo(openId);

    if (user) {

      try {
        // 检查 access_token 是否过期
        const checkResult = JSON.parse(await request(wechatConfig.checkAccessTokenUri(user.wx_access_token, openId)));

        if (checkResult.errcode !== 0) {

          // 页面跳转
          const refreshResult = JSON.parse(await request(wechatConfig.refreshTokenUri(user.wx_refresh_token)));
          console.log('refresh', refreshResult);
          if (refreshResult.errcode) {
            // 重新授权
            console.log('re auth');
            // api特殊处理
            if (/^(?:\/api\/)/.test(req.path)) {
              res.json(ErrorCreator(6000, { msg: '授权过期，请重新授权' }));
              return;
            }
            res.clearCookie('openId').redirect(wechatConfig.getCodeUri());
            return;
          }

          // 更新库
          const { access_token, refresh_token } = refreshResult;
          console.log(refreshResult);
          await User.addOrUpdateWxUserInfo({ openid: openId, access_token, refresh_token, nickname: user.wx_nickname});

          user = { ...user, wx_access_token: access_token, wx_refresh_token: refresh_token };
        }

      } catch (e) {
        res.status(500).json(ErrorCreator(3000, e));
        return;
      }

      req.wxUser = user;
      return next();
    } else {
      res.clearCookie('openId').redirect(wechatConfig.getCodeUri());
    }
  } else if (code && state) {
    try {
      const wxRes = JSON.parse(await request(wechatConfig.getAccessTokenUri(code)));

      if (wxRes.errcode) {
        return res.redirect(wechatConfig.getCodeUri());
      }

      const { access_token, refresh_token, openid } = wxRes;

      const userResult = JSON.parse(await request(wechatConfig.getUserInfoUri(access_token, openid)));

      if (!userResult) throw new Error('获取微信用户信息错误');

      const { nickname } = userResult;

      // 写库
      await User.addOrUpdateWxUserInfo({ openid, access_token, refresh_token, nickname });

      res.cookie('openId', openid, { expires: new Date(Date.now() + 86400000), httpOnly: true });
      next();
    } catch (e) {
      res.status(500).json(ErrorCreator(3000, e));
    }

  } else {
    res.redirect(wechatConfig.getCodeUri());
  }

};