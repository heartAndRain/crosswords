const express = require('express');
const path = require('path');

const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const compression = require('compression');
const serveIndex = require('serve-index');
const redisStore = require('connect-redis')(session);
const redisClient = require('./helpers/redis.helper');
const rankApi = require('./routers/rankApi');
const finalApi = require('./routers/finalTriggerApi');

app.use(compression());
app.use('/static', express.static(path.resolve(__dirname, '../client/dist')));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
  store: new redisStore({ client: redisClient }),
  name: 'sid',
  secret: 'xes-crosswords',
  // resave: true,
  // saveUninitialized: true,
  cookie: {
    httpOnly: true,
    maxAge: 86400 * 1000,
  }
}));

const Router = require('./routers');
const auth = require('./middlewares/auth');


// 微信验证文件
app.use('/MP_verify_COa7fmwquhM1DuKN.txt', (req, res) => {
  res.sendFile(path.resolve(__dirname, './MP_verify_COa7fmwquhM1DuKN.txt'));
});

// 排行榜相关
app.post('/-/crosswords/rank/trigger', rankApi.rank);

// 总决赛触发
app.post('/-/crosswords/final/trigger', finalApi.fill);

// h5
app.use(['/', '/api'], auth);
app.use('/', Router.frontEndRouter);
app.use('/api', Router.apiRouter);

// admin
const adminPath = path.resolve(__dirname, '../admin/build');
app.use('/admin/index', express.static(adminPath), serveIndex(adminPath));
app.use('/admin/api', Router.adminRouter);

let port = 4000;
console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'production') {
  port = 80;
}

app.listen(port, () => console.log(`server is Running on ${port}`));

